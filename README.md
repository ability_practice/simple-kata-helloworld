# Simple-kata-HelloWorld

## 用途

* 某项技术的系统化探究
* 编程能力训练
* CI/CD 的测试项目
* 作为其他kata项目的基础,如spring的,netty的,消息栈的,数据库的等
* 团队项目脚手架,节省搭建时间

## 特点

* TestNG 单元测试
* jacoco 覆盖率报告
* jekins 对接CI/CD
* log    日志